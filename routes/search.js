const express = require('express');
const router = express.Router();
const Team = require('../models/models').Team;
const authHelper = require('../helpers/auth-helper');

// Make sure there is a user here
router.use(authHelper.isAuthenticated);

/**
 * Search teams with a q query param
 */
router.get('/typeahead/teams', (req, res) => {
  let query = {title: new RegExp(req.query.q, 'ig')};
  let tournament = req.query.tournament;

  if(tournament) query.tournaments = [tournament];

  Team.find(query, {title: 1})
    .exec()
    .then(teams => res.json(teams))
    .catch(err => res.status(500).send(err));
});

module.exports = router;