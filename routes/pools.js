/**
 * Created by jeremydejno on 11/2/17.
 */
const express = require('express');
const router = express.Router();
const Pool = require('../models/models').Pool;
const Tournament = require('../models/models').Tournament;
const authHelper = require('../helpers/auth-helper');
const TournamentEditor = require('../helpers/tournament-editor').TournamentEditor;
const _ = require('lodash');
const SOCKET_EVENTS = require('../helpers/socket-events');
const asyncMiddleware = require('../middleware/async').asyncMiddleware;
const assert = require('assert');

// Make sure there is a user here
router.use(authHelper.isAuthenticated);

/**
 * GET pool by id.
 */
router.get('/pools/:id', asyncMiddleware(async (req, res, next) => {
  const pool = await Pool
    .findById(req.params.id)
    .populate('teams')
    .populate('matches')
    .populate({
      path: 'matches',
      populate: {path: 'teams'}
    })
    .populate({
      path: 'matches',
      populate: {path: 'refTeam'}
    })
    .exec();

  res.json(pool);
}));


/**
 * GET pools based on tournament Id TODO: assess this query for performance...
 * @desc Sends back all pools for a given tournament with matches and teams (and ref) populated.
 */
router.get('/pools/tournament/:tournamentId', (req, res, next) => {
  Tournament
    .findById(req.params.tournamentId)
    .select('divisions._id')
    .exec()
    .then(tournament => {
      Pool
        .find({division: tournament.divisions})
        .populate('matches')
        .populate({
          path: 'matches',
          populate: {path: 'teams'}
        })
        .exec()
        .then(pools => res.json(pools))
        .catch(err => res.status(500).send());
    })
    .catch(err => next(err));
});


/**
 * GET pools based on division Id
 * @desc Sends back all pools for a given division with matches and teams (and ref) populated.
 */
router.get('/pools/division/:divisionId', (req, res, next) => {
  Pool
    .find({'division': req.params.divisionId})
    .populate('matches')
    .populate({
      path: 'matches',
      populate: {path: 'teams'}
    })
    .populate({
      path: 'matches',
      populate: {path: 'refTeam'}
    })
    .exec()
    .then(pools => res.json(pools))
    .catch(err => next(err));
});

/**
 * GET pools by team.
 *
 * TODO: Make sure we use other query items here like tournamentId to make this more specific.
 *
 * Right now this assumes that a team will not be in multiple pools. Once we
 * have players and teams that exist outside of a tournament, we will have to
 * use other identifiers.
 *
 */
router.get('/pools/team/:teamId', (req, res, next) => {
  Pool
    .findOne({
      teams: req.params.teamId
    })
    .populate('division')
    .populate('matches')
    .populate({
      path: 'matches',
      populate: {path: 'teams'}
    })
    .populate({
      path: 'matches',
      populate: {path: 'refTeam'}
    })
    .exec()
    .then(pool => res.json(pool))
    .catch(err => next(err));
});

/**
 * POST to add a team to the tournament for a specific pool.
 * Using TournamentEditor, make the necessary updates to the pool, division, matches, and team to
 * save to the database.
 */
router.post('/pools/:poolId/add/team', asyncMiddleware(async (req, res, next) => {
  assert(req.params.poolId && req.body.teamTitle, 'No pool id or team title provided for adding team.');

  const pool = await TournamentEditor.addTeam(req.params.poolId, req.body.teamTitle, req.body.tournamentId);

  //add full Team Models to the pool.teams and pool.matches[*].teams before sending back
  res.json(await _addFullTeamsToPool(pool));
  socketioServer.emit(SOCKET_EVENTS.POOL_UPDATED, {poolId: pool});


}));

/**
 * DELETE to remove a team from a pool.
 * Using TournamentEditor, make the necessary updates to the division, pool, matches, and team
 * to be saved to the database.
 */
router.delete('/pools/:poolId/remove/team/:teamId', asyncMiddleware(async (req, res, next) => {
  assert(req.params.poolId && req.params.teamId, 'No pool or team id provided for removing team.');

  const pool = await TournamentEditor.removeTeam(req.params.poolId, req.params.teamId);

  //add full Team Models to the pool.teams and pool.matches[*].teams before sending back
  res.json(await _addFullTeamsToPool(pool));
  socketioServer.emit(SOCKET_EVENTS.POOL_UPDATED, {poolId: pool});

}));

/**
 * POST to reorder the teams within a pool with an array of teamIds.
 */
router.post('/pools/:poolId/reorder/team', asyncMiddleware(async (req, res, next) => {
  assert(req.params.poolId && req.body.teamIds, 'No pool or team ids provided for reordering teams.');

  const pool = await TournamentEditor.reorderTeams(req.params.poolId, req.body.teamIds);

  //add full Team Models to the pool.teams and pool.matches[*].teams before sending back
  res.json(await _addFullTeamsToPool(pool));
  socketioServer.emit(SOCKET_EVENTS.POOL_UPDATED, {poolId: pool.id});

}));

/**
 * PUT method to update a pool. Just pass the updates as the request body
 */
router.put('/pools/:id', asyncMiddleware(async (req, res, next) => {
  const pool = await Pool.findByIdAndUpdate(req.params.id, req.body, {new: true});
  res.json(await _addFullTeamsToPool(pool));
}));


/**
 * Add full Team models to pools (both team list and teams within matches)
 * @param {Document} pool - model to add the pools to.
 * @return {Promise<models.Pool>}
 * @private
 */
async function _addFullTeamsToPool(pool) {

  return await pool.populate(['matches', {
    path: 'matches',
    populate: {path: 'teams'}
  }, 'teams', 'refTeam']).execPopulate();

}

module.exports = router;