const express = require('express');
const router = express.Router();
const Tournament = require('../models/models').Tournament;
const Division = require('../models/models').Division;
const Team = require('../models/models').Team;
const authHelper = require('../helpers/auth-helper');
const TournamentEditor = require('../helpers/tournament-editor').TournamentEditor;
const _ = require('lodash');

// Make sure there is a user here
router.use(authHelper.isAuthenticated);

//#region GET

/**
 * GET array of divisions by tournament id. Populate Pools and Teams within Pools.
 */
router.get('/divisions/tournament/:id', (req, res, next) => {
  Tournament
    .findById(req.params.id)
    .populate({
      path: 'divisions',
      populate: {
        path: 'pools',
        populate: {
          path: 'matches'
        },
      }
    })
    .exec()
    .then(tournament => {
      if (!tournament) {
        res.sendStatus(404);
        return;
      }

      // Find the teams since we can't do a double nested population lookup with mongoose
      return Team.find({
        _id: {
          $in: [].concat.apply([], tournament.divisions.map((d) => d.teams))
        }
      })
        .then(teams => {
          // put the teams into the pools and pool matches
          tournament.divisions.forEach(division => {
            division.teams = division.teams.map(teamId => _.find(teams, {_id: teamId}));
            division.pools.forEach(pool => {
              pool.teams = pool.teams ? pool.teams.map(teamId => _.find(teams, {_id: teamId})) : [];
              pool.matches = _.map(pool.matches, match => {
                match.teams = _.map(match.teams, teamId => _.find(teams, {_id: teamId}));
                match.refTeam = _.find(teams, {_id: match.refTeam});
                return match;
              })
            });
          });

          res.json(tournament.divisions);
        })
    })
    .catch(err => {
      res.status(500);
      next(err);
    });
});

//#endregion


/**
 * PUT to add a pool to this division.
 * TODO: Handle game formats for added pool.
 */
router.put('/divisions/:divisionId/add/pool', (req, res, next) => {
  Division
    .findById(req.params.divisionId)
    .then(division => {
      TournamentEditor.addPool(division, req.body)
        .then(pool => res.json(pool))
        .catch(err => next(err));
    });

});

/**
 * PUT to remove a pool to this division.
 */
router.put('/divisions/:divisionId/remove/pool', (req, res, next) => {
  TournamentEditor.removePool(req.params.divisionId, req.body.poolId)
    .then(division => res.json(division))
    .catch(err => next(err));

});


/**
 * PUT method to update a pool. Just pass the updates as the request body
 */
router.put('/divisions/:id', (req, res, next) => {
  Division
    .findOneAndUpdate(
      {_id: req.params.id},
      req.body,
      {new: true})
    .then(results => res.json(results))
    .catch(err => next(err));
});


module.exports = router;