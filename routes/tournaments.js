const express = require('express');
const router = express.Router();
const Tournament = require('../models/models').Tournament;
const TournamentEditor = require('../helpers/tournament-editor').TournamentEditor;
const Team = require('../models/models').Team;
const authHelper = require('../helpers/auth-helper');
const asyncMiddleware = require('../middleware/async').asyncMiddleware;
const _ = require('lodash');


/**
 * GET all tournaments in the mongodb scoreboard database.
 */
router.get('/tournaments', authHelper.isAuthenticated, (req, res, next) => {
  Tournament
    .find()
    .populate('divisions')
    .exec()
    .then(tournaments => res.json(tournaments))
    .catch(err => {
      err.statusCode(500);
      return next(err);
    });
});

/**
 * GET all tournaments metadata.
 */
router.get('/tournaments/metadata', authHelper.isAuthenticated, (req, res, next) => {
  Tournament
    .find()
    .populate('divisions')
    .select('title dates location organizers image')
    .exec()
    .then(tournaments => res.json(tournaments))
    .catch(err => {
      err.statusCode(500);
      next(err);
    });
});

/**
 * GET a tournament by id
 */
router.get('/tournaments/:id', authHelper.isAuthenticated, (req, res, next) => {
  Tournament
    .findById(req.params.id)
    .populate('divisions')
    .populate({
      path: 'divisions.teams',
      model: 'team'
    })
    .exec()
    .then(tournament => res.json(tournament))
    .catch(err => {
      err.statusCode(500);
      next(err);
    });
});

/**
 * GET a tournament by id with all models populated
 */
router.get('/tournaments/:id/full', authHelper.isAuthenticated, (req, res, next) => {
  Tournament
    .findById(req.params.id)
    .populate({
      path: 'divisions',
      populate: {
        path: 'pools',
        model: 'pool',
        populate: {
          path: 'matches',
          model: 'match',
          populate: {
            path: 'teams',
            model: 'team'
          }
        }
      }
    })
    .exec()
    .then(tournament => {

      // Boo, we can't find the tournament
      if (!tournament) {
        res.sendStatus(404);
        return;
      }

      // Find the teams since we can't do a double nested population lookup with mongoose
      return Team.find({
        _id: {
          $in: [].concat.apply([], tournament.divisions.map((d) => d.teams))
        }
      })
        .then(teams => {
          // put the teams into the pools
          tournament.divisions.forEach(division => {
            division.teams = division.teams.map(teamId => _.find(teams, {_id: teamId}));
            division.pools.forEach(pool => {
              pool.teams = pool.teams.map(teamId => _.find(teams, {_id: teamId}));
            });
          });

          res.json(tournament);
        });
    })
    .catch(err => {
      res.sendStatus(500);
      next(err);
    });
});

/**
 * GET tournament by user id (i.e. member of teams that play in the tournament)
 */
router.get('/tournaments/member/:userId', authHelper.isAuthenticated, (req, res) => {
  Team
    .find({members: req.params.userId})
    .populate('tournaments', 'title image dates')
    .exec()
    .then(teams => {
      // If we don't have any teams, send empty list
      if (!teams) return teams;
      let tournaments = {};

      teams.forEach((team) => {
        team = team.toJSON(); // get the JSON so we can delete the tournaments key

        team.tournaments.forEach((tournament) => {
          if (!tournaments[tournament._id]) tournaments[tournament._id] = tournament;
          else tournament = tournaments[tournament._id];
          tournament.teams ? tournament.teams.push(team) : tournament.teams = [team];
        });

        //delete this from the model since we have a circular dependency that hits an infinite loop
        //during the toJSON() call since teams and tournaments have themselves as keys
        delete team.tournaments;
      });

      // Return the tournaments as an array
      return Object.values(tournaments);
    })
    .then((tournaments) => res.status(200).json(tournaments))
    .catch(err => res.status(500).send(err));
});

/**
 * GET tournaments by creator user id.
 */
router.get('/tournaments/creator/:id', (req, res) => {
  Tournament
    .find({creator: req.params.id})
    .populate('divisions')
    .exec()
    .then(tournament => res.status(200).json(tournament))
    .catch(err => {
      err.status = 500;
      next(err);
    });
});

/**
 * Remove a division from a tournament
 */
router.delete('/tournaments/:id/remove/division/:divisionId', asyncMiddleware(async (req, res, next) => {
  const tournament = await TournamentEditor.removeDivision(req.params.id, req.params.divisionId);
  res.json(tournament)
}));


/**
 * POST new Tournament
 * @returns new Tournament
 * TODO: @jeremy --> Add authentication here once we have admin roles.
 */
router.post('/tournaments', (req, res) => {
  new Tournament({
    title: req.body.title,
    organizers: req.body.organizers,
    dates: (req.body.dates instanceof Array ? req.body.dates : [res.body.dates]),
    location: req.body.location,
    sport: req.body.sport,
    divisions: req.body.divisions
  })
    .save()
    .then(tournament => res.status(200).send(tournament))
    .catch(err => res.status(500).send(err));
});

/**
 * PUT to update Tournament of id
 * @returns updated Tournament
 */
router.put('/tournaments/:id', (req, res) => {
  Tournament
    .findOneAndUpdate(
      {_id: req.params.id},
      req.body,
      {new: true})
    .then(results => res.json(results))
    .catch(err => res.status(500).send(err));

});

/**
 * PUT a new division
 */
router.put('/tournaments/:id/add/division', asyncMiddleware(async (req, res, next) => {

  const division = await TournamentEditor.addDivision(req.params.id, req.body);
  res.json(division);

}));

/**
 * DELETE a tournament wholesale
 */
router.delete('/tournaments/:id', asyncMiddleware(async (req, res, next) => {
  await TournamentEditor.deleteTournament(req.params.id);
  res.status(200).send();
}));

module.exports = router;