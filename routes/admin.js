const express = require('express');
const router = express.Router();
const csv = require('csv-stringify');
const TournamentCreator = require('../helpers/tournament-creator');
const Formidable = require('formidable');
const Upload = require('s3-uploader');
const authHelper = require('../helpers/auth-helper');

// Make sure there is a user here
router.use(authHelper.isAuthenticated);

//form data parser middleware
function tournamentFormMiddleware(req, res, next) {
  //if we are not posting, next!
  if (req.method.toLocaleUpperCase() !== 'POST')
    return next();

  //parse the request and continue on.
  const form = Formidable.IncomingForm();
  form.parse(req, (err, fields, files) => {
    if (err) return next(err);

    //parse strings that were JSON.stringified.
    //@note - benign to run JSON.parse on regular strings.
    for (let key in fields) {
      if (fields.hasOwnProperty(key) && typeof fields[key] === "string") {
        try {
          fields[key] = JSON.parse(fields[key])
        } catch(e) {
          //do nothing since we have either invalid json or a valid string, in which cause we will keep as is.
        }
      }
    }

    Object.assign(req, {fields, files});
    return next()
  });
}


/**
 * GET tournament csv template file
 */
router.get('/files/tournament/template', (req, res) => {
  let data = '';
  let file = csv({}, {
    columns: {
      'Team': 'Team',
      'Gender': 'Gender',
      'Division': 'Division',
      'Round': 'Rounds',
      'Pool': 'Pool',
      'Location': 'Location',
      'Court': 'Court',
      'StartDate': 'StartDate',
      'StartTime': 'StartTime',
      'CaptainName': 'CaptainName',
      'CaptainEmail': 'CaptainEmail',
      'MemberName': 'MemberName',
      'MemberEmail1': 'MemberEmail1',
      'MemberName2': 'MemberName2',
      'MemberEmail2': 'MemberEmail2',
      'MemberName3': 'MemberName3',
      'MemberEmail3': 'MemberEmail3',
      'MemberName4': 'MemberName4',
      'MemberEmail4': 'MemberEmail4',
      'MemberName5': 'MemberName5',
      'MemberEmail5': 'MemberEmail5',
      'MemberName6': 'MemberName6',
      'MemberEmail6': 'MemberEmail6',
      'MemberName7': 'MemberName7',
      'MemberEmail7': 'MemberEmail7',
      'MemberName8': 'MemberName8',
      'MemberEmail8': 'MemberEmail8'
    }, header: true
  });
  file.on('readable', () => {
    let row;
    while (row = file.read()) {
      data += row;
    }
  });
  file.end();
  res.send(data);
});

/**
 * POST tournament data and file.
 */
router.post('/files/tournament', tournamentFormMiddleware, (req, res) => {
  let tournamentImage = req.files['tournamentImage'];
  let tournamentFile = req.files['tournamentFile'];

  // We need a tournament file
  if (!tournamentFile) {
    res.status(500).send('CSV is required.');
    return;
  }

  // Let's upload the image first before continuing to create the tournament
  return new Promise((resolve, reject) => {
    if (tournamentImage) {
      let tournamentImageUrl = tournamentImage.path;
      let client = new Upload(process.env.AWS_S3_BUCKET_NAME, {
        aws: {
          path: 'images/',
          acl: 'public-read',
          region: process.env.AWS_S3_REGION,
          accessKeyId: process.env.AWS_S3_ACCESS_KEY_ID,
          secretAccessKey: process.env.AWS_S3_SECRET_ACCESS_KEY,
        },
        cleanup: {
          versions: true,
          original: false
        },
        versions: [{
          maxHeight: 1040,
          maxWidth: 1040,
          format: 'jpg',
          suffix: '-large',
          quality: 80
        }, {
          maxWidth: 780,
          aspect: '3:2!h',
          suffix: '-medium'
        }, {
          maxWidth: 320,
          aspect: '16:9!h',
          suffix: '-small'
        }, {
          maxHeight: 100,
          aspect: '1:1',
          format: 'png',
          suffix: '-thumb1'
        }, {
          maxHeight: 250,
          maxWidth: 250,
          aspect: '1:1',
          suffix: '-thumb2'
        }]
      });

      // Upload the file with the client and the needed information,
      // Use the first image's url as the base URL. We can append the -small, etc on the client.
      client.upload(tournamentImageUrl, {}, function (err, versions) {
        if (err) {
          reject(err);
        }

        resolve(versions[0].url);
      });
    } else {
      resolve('');
    }
  }).then((imageUrl) => {
    let tournamentMetaData = req.fields;
    if (imageUrl) tournamentMetaData.image = imageUrl;
    TournamentCreator.create(tournamentMetaData, tournamentFile)
      .then(success => res.json(success))
      .catch(err => res.status(500).send(err))
  })
    .catch(err => res.status(500).send())
});

module.exports = router;