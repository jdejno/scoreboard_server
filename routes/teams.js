const express = require('express');
const router = express.Router();
const Team = require('../models/models').Team;
const TeamTournamentMapping = require('../models/models').TeamTournamentMapping;
const authHelper = require('../helpers/auth-helper');
const asyncMiddleware = require('../middleware/async').asyncMiddleware;
const _ = require('lodash');

router.use(authHelper.isAuthenticated);

/**
 * GET a team by id
 */
router.get('/teams/:id', (req, res) => {
  Team
    .findById(req.params.id)
    .populate('members')
    .populate({
      path: 'tournaments',
      populate: {
        path: 'divisions'
      }
    })
    .exec()
    .then(team => res.json(team))
    .catch(err => res.status(500).send(err));
});

/**
 * GET teams by tournament id
 */
router.get('/teams/tournament/:tournamentId', (req, res) => {
  Team
    .find({tournaments: req.params.tournamentId})
    .populate('members')
    .exec()
    .then(teams => res.json(teams))
    .catch(err => res.status(500).send(err));
});

/**
 * GET teams by user id
 */
router.get('/teams/member/:userId', (req, res) => {
  Team
    .find({members: req.params.userId})
    .populate('members')
    .populate('tournaments', 'title image dates')
    .exec()
    .then(teams => {
      // If we don't have any teams, send empty list
      if (!teams) return teams;

      // Else, let's find the tournament mapping and return as metadata
      return TeamTournamentMapping.find({
        team: {
          $in: teams.map(team => team._id)
        }
      })
        .populate('division')
        .populate('pool')
        .exec()
        .then(teamTournamentMappings => {
          return teams.map(team => {
            team.metadata = _.find(teamTournamentMappings, {team: team._id});
            return team;
          });
        })
        .catch((err) => {
          console.log(err);
          return teams;
        });
    })
    .then(teams => res.json(teams))
    .catch(err => res.status(500).send(err));
});

/**
 * GET a team by tournamentId and memberId
 */
router.get('/teams/tournament/:tournamentId/member/:userId', (req, res) => {
  Team
    .findOne({
      tournaments: req.params.tournamentId,
      members: req.params.userId
    })
    .populate('members')
    .exec()
    .then(team => {
      // If we don't have a team, send 404 not found
      if (!team) {
        return false;
      }

      // Else, let's find the tournament mapping and return as metadata
      return TeamTournamentMapping.findOne({
        team: team._id,
        tournament: req.params.tournamentId
      })
        .populate('division')
        .populate('pool')
        .exec()
        .then(teamTournamentMapping => {
          team.metadata = teamTournamentMapping;
          return team;
        })
        .catch((err) => {
          console.log(err);
          return team;
        });
    })
    .then(team => {
      team ? res.json(team) : res.status(404).send();
    })
    .catch(err => res.status(500).send(err));
});

/**
 * Method to add a member to a team
 */
router.patch('/teams/:teamId/member/:userId/add', (req, res) => {
  Team
    .findOneAndUpdate({
        _id: req.params.teamId
      }, {
        $addToSet: {
          members: req.params.userId
        }
      },
      {
        new: true
      })
    .then(results => res.json(results))
    .catch(err => res.status(500).send(err));
});

/**
 * Method to remove a member from a team
 */
router.patch('/teams/:teamId/member/:userId/remove', (req, res) => {
  Team
    .findOneAndUpdate({
        _id: req.params.teamId
      }, {
        $pull: {
          members: req.params.userId
        }
      },
      {
        new: true
      })
    .then(results => res.json(results))
    .catch(err => res.status(500).send(err));
});


/**
 * Method to update a team. Just pass the updates as the request body
 */
router.put('/teams/:teamId', asyncMiddleware(async (req, res) => {
  const team = await Team.findOneAndUpdate({id: req.params.teamId}, req.body, {new: true}).exec();
  res.json(team);
}));

/**
 * POST new Team
 * @returns Team
 */
router.post('/teams', asyncMiddleware(async (req, res, next) => {
  const team = await new Team({title: req.body.title, tournaments: req.body.tournaments}).save();
  res.json(team);
}));

module.exports = router;