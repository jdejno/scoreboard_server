const express = require('express');
const router = express.Router();
const Match = require('../models/models').Match;
const Pool = require('../models/models').Pool;
const authHelper = require('../helpers/auth-helper');

// Make sure there is a user here
router.use(authHelper.isAuthenticated);

/**
 * GET a single match
 */
router.get('/match/:id', (req, res) => {
  Match
    .findById(req.params.id)
    .populate('teams')
    .populate('refTeam')
    .exec()
    .then(match => res.json(match))
    .catch(err => res.status(500).send());
});

/**
 * GET all matches for a team.
 */
router.get('/matches/team/:teamId', (req, res) => {
  Match
    .find({teams: req.params.teamId})
    .exec()
    .then(matches => res.json(matches))
    .catch(err => res.status(500).send(err));
});

/**
 * GET match by pool id.
 */
router.get('/matches/pool/:poolId', (req, res) => {
  Pool
    .findById(req.params.poolId)
    .populate('matches')
    .select('matches')
    .exec()
    .then(matches => res.json(matches))
    .catch(err => res.status(500).send());
});

/**
 * GET match by game id.
 */
router.get('/matches/game/:gameId', (req, res) => {
  Match
    .findOne({'games._id': req.params.gameId})
    .populate('teams')
    .exec()
    .then(match => res.json(match))
    .catch(err => res.status(500).send());
});

module.exports = router;