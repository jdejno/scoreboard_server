const express = require('express');
const router = express.Router();

/**
 * GET index.js
 */
router.get('/', (req, res) => res.render('index', {
  title: 'Tourney Sports',
  hideNav: true,
  showFooter: true
}));

router.get('/terms', (req, res) => res.render('terms', {
  title: 'Tourney Sports - Terms of Service',
  hideNav: false,
  showFooter: true
}));

router.get('/privacy', (req, res) => res.render('privacy', {
  title: 'Tourney Sports - Privacy',
  hideNav: false,
  showFooter: true
}));

router.get('/contact', (req, res) => res.render('contact', {
  title: 'Tourney Sports - Contact Us',
  hideNav: false,
  showFooter: true
}));

module.exports = router;