const express = require('express');
const router = express.Router();
const Match = require('../models/models').Match;
const authHelper = require('../helpers/auth-helper');

// Make sure there is a user here
router.use(authHelper.isAuthenticated);

//#region GETs


//#endregion


//#region POSTs

/**
 * POST score to a game. Pushes score into scores array.
 */
router.post('/games/score', (req, res, next) => {
  let score = req.body.score;
  let gameId = req.body.gameId;
  if(!gameId || !score) {
    let err = new Error('Missing required parameters.');
    err.statusCode(400);
    return next(err);
  }
  Match
    .findOneAndUpdate({'games._id': gameId}, {$push: {'games.$.scores': score}})
    .exec()
    .then(doc => res.json(doc))
    .catch(err => {
      err.statusCode(500);
      return next(err)
    });
});

/**
 * POST final score to game of match.
 */
router.post('/games/score/final', (req, res, next) => {
  let score = req.body.score;
  let gameId = req.body.gameId;
  if(!gameId || !score) {
    let err = new Error('Missing required parameters.');
    err.statusCode(400);
    return next(err);
  }
  Match
    .findOneAndUpdate({'games._id': gameId}, {
      $push: {'games.$.scores': score},
      'games.$.final': true
    })
    .exec()
    .then(doc => res.json(doc))
    .catch(err => {
      err.statusCode(500);
      return next(err)
    })
});

/**
 * POST reset score to game.
 * @desc clears the scores array for the game.
 */
router.post('/games/score/reset', (req, res, next) => {
  let gameId = req.body.gameId;
  if(!gameId) {
    let err = new Error('Missing required parameters.');
    err.statusCode(400);
    return next(err);
  }
  Match
    .findOneAndUpdate({'games._id': gameId}, {
      'games.$.scores': [],
      'games.$.final': false
    })
    .exec()
    .then(doc => res.json(doc))
    .catch(err => {
      err.statusCode(500);
      return next(err)
    })

});

//#endregion

module.exports = router;