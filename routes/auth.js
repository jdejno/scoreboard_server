const express = require('express');
const router = express.Router();
const passport = require('passport');
const Membership = require('../models/models').Membership;
const EmailMapping = require('../models/models').EmailMapping;
const Team = require('../models/models').Team;
const User = require('../models/models').User;
const facebookTokenStrategy = require('passport-facebook-token');
const _ = require('lodash');
const TokenService = require('../services/token-service');
let sendinblue = require('sendinblue-api');


/**
 * Setup Authentication with Facebook Token Strategy
 */
passport.use(new facebookTokenStrategy({
    clientID: process.env.FACEBOOK_APP_ID,
    clientSecret: process.env.FACEBOOK_APP_SECRET
  }, (accessToken, refreshToken, profile, done) => {
    Membership
      .findOne({
        providerUserId: profile.id
      })
      .exec(function (err, membershipData) {
        if (err) {
          return done(err);
        }
        //No user was found... so create a new user with values from Facebook (all the profile stuff)
        if (!membershipData) {
          let email = _.get(_.head(profile.emails), 'value', '');

          let membership = new Membership({
            provider: 'facebook',
            providerUserId: profile.id,
            accessToken: accessToken
          });
          let user = new User({
            name: profile.name,
            displayName: _.get(profile, 'displayName', ''),
            firstName: _.get(profile, 'name.givenName', ''),
            lastName: _.get(profile, 'name.familyName', ''),
            email: email,
            image: _.get(_.head(profile.photos), 'value', ''),
            sports: _.map(profile.sport, (experience) => experience.name),
            facebookInfo: profile
          });

          // Set the IDs from each of the objects so we can reverse lookup
          membership.user = user._id;
          user.memberships = [membership._id];

          // Save both.
          Promise.all([membership.save(), user.save()])
            .then((results) => {

              // Don't wait for this promise. We don't want to prevent user creation if there is an issues
              // with setting the user to teams and email mapping deletion
              _addUserToTeamsAndremoveEmailMappings(email, user._id)
                .then((team) => _emailUserConfirmation(user, team));

              done(err, results[1])
            })
            .catch((err) => {
              done(err, null);
            });
        } else {
          //found user. Return the user
          return done(err, membershipData.user);
        }
      });
  }
));

function _emailUserConfirmation(user, team) {
  let parameters = { "apiKey": process.env.SENDINBLUE_API_KEY, "timeout": 5000 };	//Optional parameter: Timeout in MS
  let sendin = new sendinblue(parameters);
}

/**
 * Adds the new user to their respective teams from their email mappings,
 * and deletes the email mappings since we now have the actual user.
 * @param email
 * @param userId
 * @returns {Promise}
 * @private
 */
function _addUserToTeamsAndremoveEmailMappings(email, userId) {
  // Try to add the new member to teams if email mappings exist
  // Then remove the email mappings because they are on the team
  return EmailMapping.find({
    email: email
  }).then((emailMappings) => {
    return Promise.all(_.map(emailMappings, emailMapping => {
      return Team.update({
        '_id': emailMapping.team
      }, {
        $push: {members: userId}
      }).then(() => {
        emailMapping.remove();
      }).catch((err) => console.log(err));
    }));
  });
}

/**
 * Middleware for generating a token on login
 * @param req
 * @param res
 * @param next
 */
function generateToken(req, res, next) {
  TokenService.createToken({user: req.user}, (err, token) => {
    if (err) return next({status: 401, err: 'User Validation failed'});
    req.genertedToken = token;
    next();
  });
}

/**
 * Route to authenticate our user session with facebook token, generate a jwt token for session auth
 */
router.post('/auth/facebook', passport.authenticate('facebook-token', {
  session: false
}), generateToken, (req, res) => {
  let user = req.user;
  if (!user) {
    res.status(401).send();
  } else {
    User.findById(user)
      .populate('memberships')
      .then((user) => res.status(200).json({user: user, token: req.genertedToken}))
      .catch((err) => res.status(401).send());
  }
});

module.exports = router;