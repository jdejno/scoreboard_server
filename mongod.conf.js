/**
 * Created by jeremydejno on 10/26/17.
 */


const config = {
  host: process.env.NODE_ENV === "development" ? "localhost" : "104.236.176.199",
  port: 27017
};

config.urls = config.urls || {};
config.urls.default = `mongodb://${config.host}:${config.port}`;
config.urls.scoreboard = `${config.urls.default}/scoreboard`;

module.exports = config;