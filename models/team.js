/**
 * Created by jeremydejno on 10/27/17.
 */
const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let options = {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
};

/**
 * Team Schema
 * @property title - Team title
 * @property members {@type Array<string>} - Array of player names and ids
 * @property tournaments {@type Array<string>} - Array of tournamentId
 */
let teamSchema = new Schema({
  title: {
    type: String,
    required: true,
    trim: true,
    index: true                   //index for team search based on title
  },
  captain: {
    type: Schema.Types.ObjectId,
    ref: 'User'                  //index for team search based on player
  },
  members: [{
    type: Schema.Types.ObjectId,
    ref: 'User',
    index: true                   //index for team search based on player
  }],
  tournaments: [{
    type: Schema.Types.ObjectId,
    ref: 'tournament',
    index: true                   //index for team search based on tournament
  }],
  record: {
    wins: Number,
    losses: Number,
    ties: Number,
    pointDifferential: Number
  }
}, options);

// Add a virtual metadata field so that I can set and return the metadata value in the API
teamSchema.virtual('metadata')
  .get(() => this._metadata)
  .set((v) => this._metadata = v);

let Team = mongoose.model('team', teamSchema);

module.exports = {Schema: teamSchema, Model: Team};