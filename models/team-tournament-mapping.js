const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

let teamTournamentMappingSchema = new Schema({
  team: {type: ObjectId, ref: 'team', required: true, index: true},
  tournament: {type: ObjectId, ref: 'tournament', required: true, index: true},
  division: {type: ObjectId, ref: 'division', required: true, index: true},
  pool: {type: ObjectId, ref: 'pool', required: true, index: true}
});

let TeamTournamentMapping = mongoose.model('TeamTournamentMapping', teamTournamentMappingSchema);

module.exports = {Schema: teamTournamentMappingSchema, Model: TeamTournamentMapping};