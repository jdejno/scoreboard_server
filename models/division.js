const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * @desc Division Schema
 * @property gender -  Gender type of the division { Men, Woman, Coed }
 * @property level - Level of the division {@example 'AA'}
 * @property team {Array} - Team id reference and rank within division.
 *
 */
const divisionSchema = new Schema({

  gender: {type: String, enum: ['Men', 'Women', 'Coed'], required: true},
  level: String,
  teams: [{
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'team',
    index: true
  }],
  pools: [{
    type: Schema.Types.ObjectId,
    ref: 'pool',
    required: true,
    index: true
  }]
});

let Division = mongoose.model('division', divisionSchema);

module.exports = {Schema: divisionSchema, Model: Division};