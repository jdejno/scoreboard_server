const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

let emailMappingSchema = new Schema({
  email: {type: String, required: true, index: true, trim: true},
  name: {type: String, trim: true},
  team: {type: ObjectId, ref: 'team', required: true},
  tournament: {type: ObjectId, ref: 'tournament', required: true}
});

let EmailMapping = mongoose.model('EmailMapping', emailMappingSchema);

module.exports = {Schema: emailMappingSchema, Model: EmailMapping};