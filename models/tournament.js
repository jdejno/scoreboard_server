/**
 * Created by jeremydejno on 10/27/17.
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let options = {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
};

/**
 * @desc Tournament Schema
 * @property title - Name of tournament
 * @property organizers - Tournament Organizers { name, company, email, phoneNumber, website }
 * @property dates - Array of Dates (of tournament)
 * @property location - Location of tournament { name, address }
 * @property divisions - Array of divisionSchemas  {@see /mongoose/models/division.js for description}
 * @property sport - Enum of sports to choose from
 */
const tournamentSchema = new Schema({
  title: {
    type: String,
    required: true,
    trim: true,
    index: true                           //index for text search lookup
  },
  organizers: [{
    name: {type: String, index: true},    //index for text search lookup
    company: String,
    email: String,
    phoneNumber: String,
    website: String
  }],
  dates: {
    type: [Date],
    required: true,
    index: true                           //index for date range lookup.
  },
  location: {
    type: {
      name: String,
      city: String,
      state: String,
      address: String
    },
    required: true
  },
  sport: {
    type: String,
    enum: ['Volleyball'],
    required: true
  },
  divisions: [{
    type: Schema.Types.ObjectId,
    ref: 'division',
    index: true
  }],
  image: {
    type: String,
    trim: true
  },
  creator: {                              //index for tournament by user id.
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'User',
    index: true
  },
  status: {
    type: String,
    enum: ['DRAFT', 'ACTIVE'],
    default: 'DRAFT',
    required: true
  }
}, options);

// Add a virtual metadata field so that I can set and return the metadata value in the API
tournamentSchema.virtual('teams')
  .get(() => {
    return this._teams
  })
  .set((t) => {
    this._teams = t
  });

let Tournament = mongoose.model('tournament', tournamentSchema);


module.exports = {Schema: tournamentSchema, Model: Tournament};