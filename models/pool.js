const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let poolSchema = new Schema({
  title: String,
  division: {
    type: Schema.Types.ObjectId,
    ref: 'division',
    index: true
  },
  teams: [{
    type: Schema.Types.ObjectId,
    ref: 'team',
    index: true
  }],
  location: {
    location: String,
    court: String
  },
  format:{
    gameScores: [{target: Number, cap: Number}]
  },
  matches: [{type: Schema.Types.ObjectId, ref: 'match'}],
  startTime: Date
});

let Pool = mongoose.model('pool', poolSchema);

module.exports = {Schema: poolSchema, Model: Pool};