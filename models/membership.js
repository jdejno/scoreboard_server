const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

let membershipSchema = new Schema({
  provider: String,
  providerUserId: String,
  accessToken: String,
  user: {type: ObjectId, ref: 'User'},
  dateAdded: {type: Date, default: Date.now}
});

let Membership = mongoose.model('Membership', membershipSchema);

module.exports = {Schema: membershipSchema, Model: Membership};