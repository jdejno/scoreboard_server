const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let gameSchema = new Schema({
  startTime: Date,
  final: Boolean,
  scores: [{
      timestamp: {type: Date, default: Date.now},
      score: [{
        team: {type: Schema.Types.ObjectId, ref: 'team', required: true},
        points: Number
      }]
    }]
});

let Game = mongoose.model("game", gameSchema);

module.exports = {Schema: gameSchema, Model: Game};