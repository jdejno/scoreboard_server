/**
 * Created by jeremydejno on 10/28/17.
 */
const Tournament = require('./tournament');
const Division = require('./division');
const Team = require('./team');
const Match = require('./match');
const Pool = require('./pool');
const Game = require('./game');
const Membership =  require('./membership');
const User =  require('./user');
const EmailMapping =  require('./email-mapping');
const TeamTournamentMapping = require('./team-tournament-mapping');


module.exports.Tournament = Tournament.Model;
module.exports.Division = Division.Model;
module.exports.Team = Team.Model;
module.exports.Match = Match.Model;
module.exports.Pool = Pool.Model;
module.exports.Game = Game.Model;
module.exports.Membership = Membership.Model;
module.exports.User = User.Model;
module.exports.EmailMapping = EmailMapping.Model;
module.exports.TeamTournamentMapping = TeamTournamentMapping.Model;
