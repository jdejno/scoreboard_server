const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

let userSchema = new Schema({
  displayName: {
    type: String,
    required: true,
    trim: true
  },
  name: {
    type: Object,
    required: true
  },
  firstName: {
    type: String,
    required: true,
    trim: true
  },
  lastName: {
    type: String,
    trim: true
  },
  email: {
    type: String,
    required: true,
    trim: true,
    index: true
  },
  image: {
    type: String,
    trim: true
  },
  gender: {
    type: String,
    trim: true
  },
  position: {type: String, trim: true},
  sports: [String],
  facebookInfo: {
    type: Object,
    required: true
  },
  memberships: [{
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Membership',
    index: true
  }]
});

let User = mongoose.model('User', userSchema);

module.exports = {Schema: userSchema, Model: User};