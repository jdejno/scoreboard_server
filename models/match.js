const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const gameSchema = require('./game').Schema;


let matchSchema = new Schema({
  teams: [{
    type: Schema.Types.ObjectId,
    ref: 'team',
    index: true
  }],
  refTeam: {type: Schema.Types.ObjectId, ref: 'team'},
  startTime: Date,
  scheduledTime: Date,
  endTime: Date,
  location: {
    gym: String,
    court: String
  },
  games: [gameSchema]
});

let Match = mongoose.model("match", matchSchema);

module.exports = {Schema: matchSchema, Model: Match};