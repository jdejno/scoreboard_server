const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

let settingsSchema = new Schema({
  shouldReceivePromotionalEmail: {
    type: Boolean,
    default: true
  },
  shouldReceiveTournamentInfoEmail: {
    type: Boolean,
    default: true
  },
  shouldReceiveEmail: {
    type: Boolean,
    default: true
  }
});

let userSettingsSchema = new Schema({
  user: {type: ObjectId, ref: 'User', required: true},
  settings: settingsSchema
});

let UserSettings = mongoose.model('UserSettings', userSettingsSchema);

module.exports = {Schema: userSettingsSchema, Model: UserSettings};