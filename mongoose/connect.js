/**
 * Created by jeremydejno on 10/26/17.
 */
const mongoose = require('mongoose');
const logger = require('../logging/logger');
const scoreboardURI = require('../mongod.conf').urls.scoreboard;
const assert = require('assert');
mongoose.Promise = global.Promise;  //using native Promise since mongoose mpromise is deprecated.

let dbFactory = function() {
    return new db();
};

let db = function () {
    this.connection = null;
};

/**
 * Connects to MongoDB scoreboard DB with desired connection options.
 * USAGE:
 *      db.connect().then(
 *          (connection) => { },
 *          (err) => {}
 *      );
 * @returns {Promise<Connection, Error>}
 */
db.prototype.connect = function () {
     return mongoose.connect(scoreboardURI, {
        useMongoClient: true,
        user: "scoreboardReadWrite",
        pass: "scoreboard",
        reconnectTries: 2,
        reconnectInterval: 500, // Reconnect every 500ms
        autoIndex: (process.env.NODE_ENV === 'development'), //autoIndex when in development environment.
        // If not connected, return errors immediately rather than waiting for reconnect
        bufferMaxEntries: 0
    }, (err, db) => {
         assert.equal(err, null);
         this.connection = db;
         logger.info(`Connected to ${scoreboardURI}`)
     });
};

db.prototype.close = function() {
  this.connection.close(() => {
      logger.info(`Disconnected from ${scoreboardURI}`);
      this.connection = null;
  })
};



module.exports = dbFactory;