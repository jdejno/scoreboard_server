const csv = require('csv-parse');
const fs = require('fs');
const models = require('../models/models');
const _ = require('lodash');
const moment = require('moment');
const poolMatchFormats = require('./tournament-helper').poolMatchFormats;


/*
Tournament Creator
 */
class TournamentCreator {

  constructor() {
  }

  /**
   * Main method. Create a tournament and populate Divisions, Pools, and Teams
   * @param metadata {Object} - metadata of the tournament.
   * @param file {File} - CSV file to be parsed containing tournament data.
   * @returns Promise<Map<string,Array<Models>>> - Promise with a map of saved models.
   *  (e.g. {'tournaments': [ <tournament1>, <tournament2>] }
   */
  create(metadata, file) {
    if (metadata === void 0 || file === void 0)
      return reject(Error('Must supply data and file for tournament creation.'));
    return this._parseCsvTemplate(file)
      .then(data => {
          let tournament = this._createTournament(metadata);
          return this._createDivisionsAndModels(data, metadata, tournament._id).then((divisionSaves) => {
            let saves;
            [tournament.divisions, saves] = divisionSaves;
            saves.push(tournament);
            return this._saveModels(saves);
          });

        }
      )
  }

  /**
   * parseCsvTemplate into use-able data.
   * @param file {File} Tournament Template file to parse.
   * @return {Promise<Object>} @type { Gender: { Division: { Pool: {Gender, Division, Pool, Team, Location, Court, StartTime} }}}
   */
  _parseCsvTemplate(file) {
    return new Promise((result, reject) => {
        fs.stat(file.path, (err, stats) => {
          if (err || !stats.isFile())
            return reject(new Error('Tournament Template File does not exist to open for parsing.'));

          let data = [];
          let parser = csv({columns: true});
          parser.on('readable', () => {
            let o;
            while (o = parser.read()) {
              // TODO Validate the row data before pushing
              data.push(o);
            }
          });

          // Catch any error
          parser.on('error', (err) => {
            reject(err.message);
          });

          let input = fs.createReadStream(file.path);
          input.pipe(parser);
          input.on('end', () => {
            input.close();
            result(data);
          });

        });
      }
    )
  }

  /**
   * Create Division, Pool, and Team models.
   * @param csvData {Array<Object>} - parsed data from CSV Template { Genders: { Divisions: { Pools: { Gender, Division, Pool, Team, Location, Court, StartTime} }}}
   * @param tournamentMetadata {Object} - metadata of the tournament. This is needed for pool.format.gameScores.
   * @param tournamentId {String}
   * @private
   * @returns {Array<Array<Model>>  } divisionModels and saveModels
   *  - divisionModels for assigning to tournament.
   *  - saveModels for saving to mongoDB.
   */
  _createDivisionsAndModels(csvData, tournamentMetadata, tournamentId) {
    let divisionModels = {};
    let poolModels = {};
    let roundsMap = {};
    let teamModels = [];
    let matchModels = [];
    let saveModels = [];
    let generatedEmailMappings = [];
    let generatedTeamTournamentMappings = [];

    // Create Divisions, Pools, and Teams
    csvData.map(row => {
      let divisionModel = divisionModels[row.Gender + row.Division];
      if (!divisionModel) {
        divisionModel = this._createDivision(row.Gender, row.Division);
        divisionModels[row.Gender + row.Division] = divisionModel;
      }

      // //add rounds to division
      // let roundIdx = roundsMap[row.Round + row.Division + row.Gender];
      // if(roundIdx === null || roundIdx === undefined) {
      //   //round
      //   roundIdx = divisionModel.rounds.push({
      //     title: row.Round,
      //     dates: [this._getDateTime(row.StartDate, row.StartTime)],
      //     pools: []
      //   }) - 1;
      //   roundsMap[row.Round + row.Division + row.Gender] = roundIdx;
      // }

      let poolModel = poolModels[row.Gender + row.Division + row.Pool];
      if (!poolModel) {
        poolModel = this._createPool(divisionModel, row.Pool, row.Location, row.Court, this._getDateTime(row.StartDate, row.StartTime), tournamentMetadata);
        poolModels[row.Gender + row.Division + row.Pool] = poolModel;
        divisionModel.pools.push(poolModel);
        // divisionModel.rounds[roundIdx].pools.push(poolModel);
      }

      let teamModel = this._createTeam(row.Team, tournamentId);
      generatedEmailMappings = _.concat(generatedEmailMappings, this._generateEmailMappings(row, teamModel._id, tournamentId));

      poolModel.teams ? poolModel.teams.push(teamModel.id) : poolModel.teams = [teamModel.id];
      divisionModel.teams ? divisionModel.teams.push(teamModel.id) : divisionModel.teams = [teamModel.id];

      teamModels.push(teamModel);
      generatedTeamTournamentMappings.push(this._generateTeamTournamentMapping(teamModel._id, tournamentId, divisionModel._id, poolModel._id));
    });

    // create and set matches for the pools
    _.each(Object.values(poolModels), poolModel => {
      let location = _.get(poolModel, 'location.location');
      let court = _.get(poolModel, 'location.court');
      let matches = this._createMatchesByTeams(poolModel.teams, location, court, tournamentMetadata, poolModel.startTime);
      poolModel.matches = _.map(matches, match => match.id);
      matchModels = _.concat(matchModels, matches);
    });

    // Map existing users to teams via email
    return this._mapUsersToTeams(generatedEmailMappings, teamModels).then(() => {
      saveModels = _.concat(saveModels, Object.values(divisionModels), Object.values(poolModels), teamModels, matchModels, generatedEmailMappings, generatedTeamTournamentMappings);
      return [Object.values(divisionModels).map((division) => division._id), saveModels];
    });
  }

  /**
   * Create a Tournament Model from metadata.
   * Must populate necessary reference Models after calling this method.
   * @param tournamentMetadata - metadata of the tournament.
   * @private
   * @return {Tournament.Model}
   */
  _createTournament(tournamentMetadata) {
    return new models.Tournament({
      title: tournamentMetadata['tournamentTitle'],
      dates: this._getDateRange(_.get(tournamentMetadata, 'dates.start'), _.get(tournamentMetadata, 'dates.end')),
      location: {
        name: _.get(tournamentMetadata, 'location.name'),
        city: _.get(tournamentMetadata, 'location.city'),
        state: _.get(tournamentMetadata, 'location.state'),
        address: _.get(tournamentMetadata, 'location.address'),
      },
      sport: 'Volleyball',
      divisions: [],
      image: tournamentMetadata['image'],
      creator: _.get(tournamentMetadata.creator, '_id', tournamentMetadata.creator)
    });
  }

  /**
   * Create Division from template data.
   * @param gender - Men, Women, or Coed
   * @param level - Level of division (can be anything, but typically A, AA, B, etc.)
   * @private
   */
  _createDivision(gender, level) {
    return new models.Division({
      gender: _.upperFirst(gender),
      level: _.upperCase(level),
      pools: [],
      teams: [],
      rounds: []
    })
  }

  /**
   * Create a Pool from array of template data.
   * @param divisionModel {Division.Model} - Division to add id of.
   * @param title
   * @param location
   * @param court
   * @param startTime {Date}
   * @param metadata {Object} - metadata about tournament. Needed for pool.format.gameScores
   * @returns {Pool.Model}
   * @private
   */
  _createPool(divisionModel, title, location, court, startTime, metadata) {
    return new models.Pool({
      title: title ? title.replace(/(pools*\s)/ig, '') : '', //replace any instance of '[pP]ool[s]' from our title with ''.
      division: divisionModel._id,
      location: {
        location: location,
        court: court
      },
      format: {
        gameScores: _.get(metadata, 'format.gameScores')
      },
      startTime: startTime
    });
  }

  /**
   * Create Array of Teams from array of template data..
   * @param title {String}
   * @param tournamentId {String}
   * TODO: Not current persisting teams through tournaments, but will need to rethink this once we do.
   * @private
   */
  _createTeam(title, tournamentId) {
    return new models.Team({
      title: title,
      tournaments: [tournamentId]
    });
  }

  /**
   * Generates a nice mapping of the team/tournament/division/pool so we can easily look up
   * @returns {TeamTournamentMapping.Model}
   * @private
   */
  _generateTeamTournamentMapping(teamId, tournamentId, divisionId, poolId) {
    return new models.TeamTournamentMapping({
      team: teamId,
      tournament: tournamentId,
      division: divisionId,
      pool: poolId
    });
  }

  /**
   * Generate Email mappings so we can send emails
   * @param row
   * @param teamId
   * @param tournamentId
   * @returns {Array}
   * @private
   */
  _generateEmailMappings(row, teamId, tournamentId) {
    let emailMappings = [];
    let email = row.CaptainEmail;
    let name = row.CaptainName;
    let i = 1;

    if (!teamId) throw new Error('Cannot create an email mapping without teamId');
    if (!email) throw new Error('Needs Captain Email');

    // Push captains mapping
    emailMappings.push(new models.EmailMapping({
      email: email,
      name: name,
      team: teamId,
      tournament: tournamentId
    }));

    // Push other team members mapping
    while (email) {
      email = row[`MemberEmail${i}`];
      name = row[`MemberName${i}`];
      if (email) {
        emailMappings.push(new models.EmailMapping({
          email: email,
          name: name,
          team: teamId,
          tournament: tournamentId
        }));
      }
      i++;
    }

    return emailMappings;
  }

  /**
   * Gets users by their email addresses and adds their userIds to their teams
   * We will use this to populate their selected team in the tournament
   * @param emailMappings
   * @param teams
   * @returns {Promise.<TResult>|Promise|*}
   * @private
   */
  _mapUsersToTeams(emailMappings, teams) {
    return this._getUsersByEmail(emailMappings.map((emailMapping) => emailMapping.email))
      .then((users) => {
        users.forEach((user) => {
          let emailMapping = _.find(emailMappings, {email: user.email});
          let team = _.find(teams, {_id: emailMapping.team});
          team.members ? team.members.push(user._id) : team.members = [user._id];
          _.remove(emailMappings, {email: user.email});
        });
      });
  }

  /**
   * Runs a simple query to get users by their email from Mongo.
   * @param emailArray
   * @returns {Query|T|*}
   * @private
   */
  _getUsersByEmail(emailArray) {
    return models.User.find({
      'email': {
        $in: emailArray
      }
    });
  }

  /**
   * Create matches based on number of teams in the pool
   * @param teams {Array<Team.Model>} - teams in a single Pool.
   * @param location {String}
   * @param court {String}
   * @param metadata {Object} - metadata of the tournament. This is needed for games (number of games)
   * @param startDateTime {Date} - Start time of the pool
   * @private
   * @todo figure out good solution for scheduledTime based on pool start time.
   */
  _createMatchesByTeams(teams, location, court, metadata, startDateTime) {
    //TODO: This may be a poor assumption for start time.... Best we got with CSV to match level granularity
    let format = poolMatchFormats[teams.length];
    let numOfGames = _.get(metadata, 'format.numOfGames', 3);
    return _.map(format, (teamMap, i) => {
      return new models.Match({
        teams: [teams[teamMap[0]], teams[teamMap[1]]],
        refTeam: teams[teamMap[2]],
        scheduledTime: startDateTime ? this._getMatchStartTimeFromPoolStartTime(startDateTime, i) : null,
        games: this._createMatchGames(numOfGames),
        location: {
          location: location,
          court: court
        }
      })
    });
  }

  /**
   * Create Games for Match Model. Create number of games indicated.
   * @param numOfGames {Number} - number of games to create.
   * @private
   */
  _createMatchGames(numOfGames) {
    let games = [];
    _.times(numOfGames, () => {
      games.push(new models.Game({}));
    });
    return games;
  }

  //#region Private helpers

  /**
   * Save Models to MongoDB using mongoose.save(). Also, validates before we actually save.
   * We don't want any rogue items.
   * @param models {Array<Model>} models to be saved to the database
   * @returns {Promise<data>} - Promise resolved to Object map of models.toJSON saved.
   * @private
   * @interface data {
   *    tournaments: Array<Tournament.Model>,
   *    pools: Array<Pool.Model>,
   *    teams: Array<Team.Model>,
   *    matches: Array<Match.Model>
   *  }
   */
  _saveModels(models) {
    return Promise.all(_.map(models, model => model.validate()))
      .then(() =>
        Promise.all(_.map(models, model => {
            return model.save().then(model => {
              let o = {};
              o[model.collection.name] = model;
              return o;
            }).catch((e) => {
              console.log(e);
            })
          })
        ))
      .then(values => this._postSaveTransform(values))
      .catch((e) => console.log(e, 'Failure to validate or save models'));
  }

  /**
   * Array<{collection.name: document}>
   * @param models
   * @private
   * @returns
   * data
   * {
   *    tournaments: Array<Tournament.Model>,
   *    divisions: Array<Division.Model>,
   *    pools: Array<Pool.Model>,
   *    teams: Array<Team.Model>,
   *    matches: Array<Match.Model>
   *  }
   */
  _postSaveTransform(models) {
    return _
      .chain(models)
      .groupBy(_.keys)
      .mapValues(docs => _.flatMap(docs, _.values))
      .value();
  }

  /**
   * Get an Array of Dates from the given start and end dates.
   * @param start {Date} - start date (inclusive)
   * @param end {Date} - end date (inclusive)
   * @returns {Array<Date>} - Array of dates sorted earliest to latest
   * @private
   */
  _getDateRange(start, end) {
    start = moment(start).hours(0).minutes(0).seconds(0).milliseconds(0);
    end = moment(end).hours(0).minutes(0).seconds(0).milliseconds(0);

    let dates = [];
    let currentDate = start;
    while (currentDate <= end) {
      dates.push(moment(currentDate));
      currentDate.add(1, 'days');
    }
    return dates;
  }

  /**
   * Increment the given start time by x hours.
   * @param datetime - date time to increment.
   * @param increment - number of hours to increment
   * @return {moment.Moment}
   * @private
   */
  _getMatchStartTimeFromPoolStartTime(datetime, increment) {
    return moment(datetime).add(increment, 'hours');
  }

  /**
   * Create Moment object from date and time strings. Handle yuckiness.
   * @param dateString
   * @param timeString
   * @return {Date | null}
   * @private
   */
  _getDateTime(dateString, timeString) {
    let date = moment(`${dateString} ${timeString}`, ['MM-DD-YY h:m:s A', 'MM-DD-YY h:m A']);
    return date.isValid() ? date : null;
  }


  //#endregion
}

module.exports = (function () {
  return new TournamentCreator()
})();



