const Pool = require('../models/models').Pool;
const Match = require('../models/models').Match;
const Game = require('../models/models').Game;
const Team = require('../models/models').Team;
const Division = require('../models/models').Division;
const Tournament = require('../models/models').Tournament;
const _ = require('lodash');
const moment = require('moment');
const EventEmitter = require('events');
const logger = require('../logging/logger');
const poolMatchFormats = require('./tournament-helper').poolMatchFormats;


/**
 * Responsible for editing all things related to the Pool Model.
 */
class PoolEditor {

  constructor() {}

  //#region Public methods

  /**
   * Create a new pool model.
   * @param pool - pool to add.
   * @param divisionId - id of division to add pool to.
   * @return {Pool.Model}
   */
  async createPool(pool, divisionId) {
    return await new Pool({
      title: pool.title,
      startTime: pool.startTime,
      location: pool.location,
      format: {
        gameScores: _.get(pool, 'format.gameScores', [])
      },
      division: divisionId
    }).save();
  }

  async deletePool(poolId) {
    return await Pool.findByIdAndRemove(poolId).exec();
  }

  /**
   * Given a pool, add the team to the pool and handle any reformatting to the pool.
   * NOTE: Team is not saved to DB within this method.
   * @param poolId
   * @param teamId
   * @return {Promise<Pool>}
   */
  async addTeam(poolId, teamId) {

    const pool = await Pool.findByIdAndUpdate(poolId, {$push: {teams: teamId}}, {new: true}).exec();

    return await this._processAndSavePoolMatchReformat(pool);

  }

  /**
   * Remove team from pool and reformat the match schedule.
   * @param pool
   * @param teamId
   * @return {Promise<Pool>}
   */
  async removeTeam(poolId, teamId) {

    const pool = await Pool.findByIdAndUpdate(poolId, {$pull: {teams: teamId}}, {new: true}).exec();

    return await this._processAndSavePoolMatchReformat(pool);

  }

  /**
   * Reorder the teams within a pool.
   * @param poolId
   * @param teamIds - array of team ids to use for reordering.
   * @return {*}
   */
  async reorderTeams(poolId, teamIds) {

    const pool = await Pool.findByIdAndUpdate(poolId, {$set: {teams: teamIds}}, {new: true}).exec();

    return await this._processAndSavePoolMatchReformat(pool);

  }

  //#endregion

  //#region Private Helpers

  /**
   * Given a pool, reformat the matches based on the order/number of teams in Pool.teams.
   * Remove old matches, save new matches, and save the pool before resolving the promise with the updated pool.
   * @param {Document} pool - pool to be processed and saved.
   * @return {Promise<Pool>} - Pool with updated matches.
   * @private
   */
  async _processAndSavePoolMatchReformat(pool) {

    //delete old matches
    Match.remove({_id: {$in: _.clone(pool.matches)}}).exec();

    //create and set new matches in pool
    let newMatches = this._poolReformatMatchSchedule(pool);
    Promise.all(_.map(newMatches, match => match.save()));
    pool.set('matches', _.map(newMatches, '_id'));

    try {
      return await pool.save();
    } catch (err) {
      return err;
    }
  }

  /**
   * Given a pool, create matches based on Pool.teams.
   * DOES NOT SAVE TO DB, ONLY CREATES.
   * @param pool
   * @return {Array<Document>} - returns newly created matches to be assigned to Pool.matches.
   * @private
   */
  _poolReformatMatchSchedule(pool) {
    let teams = pool.teams;
    let schedule = poolMatchFormats[_.size(teams)];
    let startDateTime = pool.startTime;
    let numOfGames = _.size(_.get(pool, 'format.gameScores'));

    return _.map(schedule, (teamMap, i) => {
      return new Match({
        teams: [teams[teamMap[0]], teams[teamMap[1]]],
        refTeam: teams[teamMap[2]],
        scheduledTime: startDateTime ? this._getMatchStartTimeFromPoolStartTime(startDateTime, i) : null,
        games: this._createMatchGames(numOfGames),
        location: {
          location: _.get(pool, 'location.location'),
          court: _.get(pool, 'location.court')
        }
      });
    });

  }

  /**
   * Create Games for Match Model. Create number of games indicated.
   * @param numOfGames {Number} - number of games to create.
   * @private
   */
  _createMatchGames(numOfGames) {
    let games = [];
    _.times(numOfGames, () => {
      games.push(new Game({}));
    });

    return games;
  }

  /**
   * Increment the given start time by x hours.
   * @param datetime - date time to increment.
   * @param increment - number of hours to increment
   * @return {moment.Moment}
   * @private
   */
  _getMatchStartTimeFromPoolStartTime(datetime, increment) {
    return moment(datetime).add(increment, 'hours');
  }

  //#endregion


}

/**
 * Responsible for updating all things related to the Division Model.
 */
class DivisionEditor {

  constructor() {}

  //#region Public Methods

  /**
   * Add a team to a division.
   * @param {ObjectId} divisionId - id of division to add to.
   * @param {ObjectId} teamId -  team to add.
   */
  async addTeam(divisionId, teamId) {
    return await Division.findOneAndUpdate({_id: divisionId}, {$push: {teams: teamId}}).exec();

  }

  /**
   * Remove a team from Division.teams
   * @param divisionId
   * @param teamId
   * @return {Promise}
   */
  async removeTeam(divisionId, teamId) {
    return await Division.findOneAndUpdate({_id: divisionId}, {$pull: {teams: teamId}}).exec();
  }

  /**
   * Add Pool model to division.
   * @param division - Division.Model to update
   * @param pool - Pool.Model to add to division.
   */
  async addPool(division, pool) {
    division;
  }

  /**
   * Remove the pool from the list of pools within the division.
   * @param divisionId - id of division to remove the pool from.
   * @param pool - Pool.Model of pool being deleted.
   * @returns {Promise<Division.Model>}
   */
  async removePool(divisionId, pool) {
    return await Division.findOneAndUpdate(divisionId,
      {$pull: {pools: pool._id, teams: {$in: pool.teams}}},
      {new: true}
    ).exec();

  }

  //#endregion

}


class TournamentEditor {

  constructor() {
    this.poolEditor = new PoolEditor();
    this.divisionEditor = new DivisionEditor();
  }

  //#region Teams

  /**
   * Add a team to a pool within a tournament.
   * @param {Pool} poolId
   * @param {string} teamTitle
   * @param {string} tournamentId
   * @returns models.Pool including the full teams populated for pool.teams and pool.matches[*].teams
   */
  async addTeam(poolId, teamTitle, tournamentId) {

    const team = await new Team({title: teamTitle, tournaments: [tournamentId]}).save();

    const pool = await this.poolEditor.addTeam(poolId, team._id);

    this.divisionEditor.addTeam(pool.division, team._id);

    return pool;

  }

  /**
   * Remove a team from a tournament. Both from the pool and the division.
   * @param {string} poolId - pool to remove the team from.
   * @param {string} teamId - team id of team to remove from pool.
   * @return {Promise<Pool>}
   */
  async removeTeam(poolId, teamId) {

    Team.findByIdAndRemove(teamId).exec();

    const pool = await this.poolEditor.removeTeam(poolId, teamId);
    this.divisionEditor.removeTeam(pool.division, teamId);

    return pool;

  }

  /**
   * Reorder the teams within a pool.
   * @param {string} poolId - pool to update
   * @param {Array<string>} teamIds - array of team ids to use for reordering.
   * @return {*}
   */
  async reorderTeams(poolId, teamIds) {
    return await this.poolEditor.reorderTeams(poolId, teamIds);
  }

  //#endregion

  //#region Pools


  /**
   * Add a new pool with minimal data to division
   * @param divisionId
   * @param poolData - pool data {title, startTime, location}.
   * @returns {Pool.Model}
   */
  async addPool(divisionId, poolData) {
    const pool = await this.poolEditor.createPool(poolData, divisionId);
    Division
      .findByIdAndUpdate(divisionId,
        {$push: {pools: pool.id}}).exec();
    return pool;
  }


  /**
   * Remove a pool from a division and delete it.
   * @param divisionId - id of the division to remove the pool from.
   * @param poolId - id of pool to remove.
   * @returns {Promise<Division.Model>}
   */
  async removePool(divisionId, poolId) {
    const pool = await this.poolEditor.deletePool(poolId);
    return await this.divisionEditor.removePool(divisionId, pool);

  }

  //#endregion

  //#region Divisions

  /**
   *
   * @param tournamentId
   * @param divisionData
   * @returns {Promise<void>}
   */
  async addDivision(tournamentId, divisionData) {

    const division = await new Division({
      gender: divisionData.gender,
      level: divisionData.level,
      pools: divisionData.pools || [],
      teams: divisionData.teams || []
    }).save();

    Tournament.findByIdAndUpdate(tournamentId, {$push: {divisions: division._id}}).exec();

    return division;

  }

  /**
   * Remove a division. Delete and remove from tournament.
   * @param tournamentId
   * @param divisionId
   */
  async removeDivision(tournamentId, divisionId) {
    const division = await Division.findByIdAndRemove(divisionId).exec();

    Pool.remove({_id: {$in: division.pools}}).exec();
    Team.remove({_id: {$in: division.teams}}).exec();

    return await Tournament.findByIdAndUpdate(tournamentId, {$pull: {divisions: division._id}}).exec();

  }

  //#endregion


  //#region Tournaments

  /**
   * Delete an entire tournament
   * @param tournamentId
   * @returns {Promise<*>}
   */
  async deleteTournament(tournamentId) {
    //delete the tournament and divisions
    const tournament = await Tournament.findByIdAndRemove(tournamentId).exec();
    const divisions = await Division.remove({_id: {$in: tournament.divisions}});


    const {poolIds, teamIds} = _.reduce(divisions, (acc, div) => {
      acc.pools.concat(div.pools);
      acc.teams.concat(div.teams);
    }, {pools: [], teams: []});

    Team.remove({_id: {$in: teamIds}}).exec();

    const pools = await Pool.remove({_id: {$in: poolIds}}).exec();

    const matchIds = _.flatMap(pools, pool => pool.matches);
    Match.remove({_id: {$in: matchIds}}).exec();

    return tournament;
  }

  //#endregion

}

module.exports = {
  TournamentEditor: new TournamentEditor()
};