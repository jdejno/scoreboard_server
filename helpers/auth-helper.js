/**
 * Helper file with authentication helpers
 */

/**
 * Setting up middleware for user authentication checks
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function isAuthenticated(req, res, next) {
  if (req.isAuthenticated() && req.user) return next();
  else res.sendStatus(401);
}

module.exports = {
  isAuthenticated: isAuthenticated
};