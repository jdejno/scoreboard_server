/**
 * Tournament Creator and Editor helpers that span both classes.
 */

/**
 * Match order map. [[team 1, team 2, ref team], ... ]
 * @type {{4: [*]}}
 * @private
 */
const poolMatchFormats = {
  2: [[0, 1]],
  3: [[0,2,1], [1,2,0], [0,1,2]],
  4: [[0, 2, 3], [1, 3, 2], [0, 3, 1], [1, 2, 0], [2, 3, 1], [0, 1, 3]]
};

module.exports = {
  poolMatchFormats: poolMatchFormats
};