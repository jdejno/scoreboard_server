const jwt = require('jsonwebtoken');

/**
 * Service that allows us to use a token instead of sessions for auth
 */
class TokenService {
  constructor(headers) {
    this.token      = this._extractTokenFromHeaders(headers);
    this.payload    = {};
    this.validToken = false;

    this._verifyToken();
  }

  /**
   * Create a token and call a callback
   * @param options
   * @param cb
   */
  static createToken(options, cb) {
    const payload = {
      _id: options.user
    };

    jwt.sign(payload, process.env.TOKEN_SECRET, {
      algorithm: 'HS256'
    }, cb);
  }

  getPayload() {
    return this.payload;
  }

  /**
   * Is the user logged in?
   * @returns {boolean}
   */
  isAuthenticated() {
    return this.validToken;
  }

  /**
   * Check to see if the token is signed correctly
   * @private
   */
  _verifyToken() {
    if(!this.token || this.token == 'undefined') return;

    try {
      this.payload    = jwt.verify(this.token, process.env.TOKEN_SECRET);
      this.validToken = true;
    } catch (err) {
      this.payload    = {};
      this.validToken = false;
      console.log(err);
      console.log('token:', this.token);
    }
  }

  /**
   * Get the token
   * @param headers
   * @returns {*}
   * @private
   */
  _extractTokenFromHeaders(headers) {
    if(!headers || !headers.authorization) return false;

    return headers.authorization.replace(/^(Bearer)\s*/, '');
  }
}

module.exports = TokenService;