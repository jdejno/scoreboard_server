const express = require('express');
const path = require('path');
const helmet = require('helmet');
const morgan = require('morgan');
const logger = require('./logging/logger');
const bodyParser = require('body-parser');
const passport = require('passport');
const TokenService = require('./services/token-service');

//Controllers
const tournamentController = require('./routes/tournaments');
const teamController = require('./routes/teams');
const searchController = require('./routes/search');
const matchController = require('./routes/match');
const poolController = require('./routes/pools');
const divisionsController = require('./routes/divisions');
const gameController = require('./routes/game');
const adminController = require('./routes/admin');
const authController = require('./routes/auth');

//Routes
const indexRouter = require('./routes/index');

const app = express();

// Security for typical attacks
app.use(helmet());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.set('etag', false); // Prevents 304 Status Code during dev

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(morgan('dev', {stream: logger.stream}));  //stream morgan logging to our winston logger.
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));

// Allow cross-origin
app.use(function (req, res, next) {
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Origin', req.headers.origin);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, PATCH, OPTIONS');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Max-Age', '86400'); // 24hr
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization');
    res.sendStatus(200);
  } else {
    res.header('Access-Control-Allow-Origin', req.headers.origin);
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    res.header('Access-Control-Allow-Credentials', true);
    return next();
  }
});

/**
 * Token deserialization
 * here we will check for token existence and extract the user payload
 * We will use the payload on other routes down the request pipeline
 *
 * We are moving away from sessions because they don't 100% work with ionic on iOS/Android
 */
app.use((req, res, next) => {
  const token = new TokenService(req.headers);

  req.isAuthenticated = (token.isAuthenticated).bind(token);
  req.tokenPayload = token.getPayload();
  req.user = {_id: req.tokenPayload._id};

  next();
});

// Initialize Passport
app.use(passport.initialize());


/**
 * View Routes
 */
app.use('/', indexRouter);


/**
 * Custom routs for the API
 */
const appControllers = [
  authController,
  tournamentController,
  teamController,
  searchController,
  matchController,
  poolController,
  divisionsController,
  gameController
];
appControllers.forEach(controller => app.use('/api', controller.router || controller));

/**
 * Admin Controllers
 */
const adminControlllers = [
  adminController
];
adminControlllers.forEach(controller => app.use('/admin/api', controller));

//#endregion


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  const error = process.env.NODE_ENV === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({error});
});


module.exports = app;
