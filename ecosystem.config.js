module.exports = {
  apps : [{
    name   : "tourney",
    script : "./bin/www",
    env: {
      NODE_ENV: 'development'
    }
  }]
};
