/**
 * Created by jeremydejno on 10/26/17.
 */
const winston = require('winston');
let DailyLogger = require('winston-daily-rotate-file');
const path = require('path');
const fs = require('fs');

const logDir = path.join(__dirname, 'logs');

if(!fs.existsSync(logDir)){
  fs.mkdirSync(logDir)
}


/**
 * @desc Custom logger using winston for daily rotating logging to files (info and errors) and to Console with color.
 */
const logger = new winston.Logger({
    transports: [
        //log info to ./logs/<currentDate>-info.log file
        new DailyLogger({
            name: 'info-file',
            level: process.env.NODE_ENV === 'development' ? 'debug' : 'info', //verbose in development env
            filename: path.join(logDir, `-info.log`),
            datePattern: 'yyyy-MM-dd',
            prepend: true,
            json: true
        }),
        //log info to ./logs/<currentDate>-errors.log file
        new DailyLogger({
            name: 'error-file',
            level: 'warn',
            filename: path.join(logDir,`-errors.log`),
            datePattern: 'yyyy-MM-dd',
            prepend: true,
            json: true
        }),
        //log debug info to console (with color)
        new winston.transports.Console({
            level: 'debug',
            handleExceptions: true,
            colorize: true,
            stringify: (obj) => JSON.stringify(obj),
            json: false
        })
    ],
    exitOnError: false
});

//allow streaming from morgan logger to winston level info
logger.stream = {
    write: (message, encoding) => {
        logger.info(message.trim())
    }
};

module.exports = logger;