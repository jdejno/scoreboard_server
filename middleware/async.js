/**
 * Async Middleware
 */

/**
 * Async middleware for easily handling errors without the need of a 'catch' call.
 * @param fn - async function to resolve to Promise
 * @returns {function(*=, *=, *=)}
 */
const asyncMiddleware = fn =>
  (req, res, next) => {
    Promise.resolve(fn(req, res, next))
      .catch(next);
  };

module.exports.asyncMiddleware = asyncMiddleware;